################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../system/src/freeRTOSc/croutine.c \
../system/src/freeRTOSc/event_groups.c \
../system/src/freeRTOSc/heap_4.c \
../system/src/freeRTOSc/list.c \
../system/src/freeRTOSc/port.c \
../system/src/freeRTOSc/queue.c \
../system/src/freeRTOSc/tasks.c \
../system/src/freeRTOSc/timers.c 

OBJS += \
./system/src/freeRTOSc/croutine.o \
./system/src/freeRTOSc/event_groups.o \
./system/src/freeRTOSc/heap_4.o \
./system/src/freeRTOSc/list.o \
./system/src/freeRTOSc/port.o \
./system/src/freeRTOSc/queue.o \
./system/src/freeRTOSc/tasks.o \
./system/src/freeRTOSc/timers.o 

C_DEPS += \
./system/src/freeRTOSc/croutine.d \
./system/src/freeRTOSc/event_groups.d \
./system/src/freeRTOSc/heap_4.d \
./system/src/freeRTOSc/list.d \
./system/src/freeRTOSc/port.d \
./system/src/freeRTOSc/queue.d \
./system/src/freeRTOSc/tasks.d \
./system/src/freeRTOSc/timers.d 


# Each subdirectory must supply rules for building sources it contributes
system/src/freeRTOSc/%.o: ../system/src/freeRTOSc/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=softfp -mfpu=fpv4-sp-d16 -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -flto -Wunused -Wuninitialized -Wall -Wextra -Wmissing-declarations -Wconversion -Wpointer-arith -Wpadded -Wshadow -Wlogical-op -Waggregate-return -Wfloat-equal  -g -DSTM32F429xx -DUSE_HAL_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f4-hal" -I../system/include/freeRTOSh -std=gnu11 -Wmissing-prototypes -Wstrict-prototypes -Wbad-function-cast -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


