/*
 * Hc05.cpp
 *
 *  Created on: 26 Haz 2015
 *      Author: hasip.tuna
 */

#include "Hc05.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_usart.h"
#include "stm32f4xx_hal_dma.h"
#include "stm32f4xx_hal_rcc.h"

Hc05::Hc05(uint8_t transmitBuffersize, uint8_t recieveBufferSize) {
	// TODO Auto-generated constructor stub
	hardwareInit();
	_transmitBuffer = new int8_t[transmitBuffersize];
	_recieveBuffer  = new int8_t[recieveBufferSize];

	_transmitBufferLen = transmitBuffersize;
	_recieveBufferLen  = recieveBufferSize;
}

Hc05::~Hc05() {
	// TODO Auto-generated destructor stub
}
/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
void Hc05::hardwareInit(void){
	UART_HandleTypeDef 	huart1;
	GPIO_InitTypeDef 	GPIO_InitStruct;
	__GPIOA_CLK_ENABLE();
	__USART1_CLK_ENABLE();

    GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_10;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	huart1.Instance = USART1;
	huart1.Init.BaudRate = 9600;
	huart1.Init.WordLength = UART_WORDLENGTH_8B;
	huart1.Init.StopBits = UART_STOPBITS_1;
	huart1.Init.Parity = UART_PARITY_NONE;
	huart1.Init.Mode = UART_MODE_TX_RX;
	huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart1.Init.OverSampling = UART_OVERSAMPLING_16;
	HAL_UART_Init(&huart1);


}

/*
 * @brief         :
 * @param[]       :
 * @return        :
 * @precondition  :
 * @postcondition :
 */
