/*
 * Hc05.h
 *
 *  Created on: 26 Haz 2015
 *      Author: hasip.tuna
 */

#ifndef HC05_H_
#define HC05_H_
#include "stdint.h"

//Master
class Hc05 {
public:
	enum Baudrate{BAUD_9600, BAUD_19200, BAUD_38400, BAUD_57600, BAUD_115200};
	enum Stopbits{STOP_1, STOP_2};
	enum Parity{NONE, ODD, EVEN};
	enum Modes{MASTER, SLAVE};

	Hc05(uint8_t transmitBuffersize, uint8_t recieveBufferSize);
	//Hc05(Hc05::Baudrate baud, Hc05::Stopbits stopbit, Hc05::Parity parity);
	virtual ~Hc05();

	void setBaud(Hc05::Baudrate baud);
	void setStopbit(Hc05::Stopbits stopbit);
	void setParity(Hc05::Parity parity);
	void configureHC05(Hc05::Modes mode);

	void setName(int8_t const *name, uint8_t len);
	void setPassword(int8_t const *pass, uint8_t len);

	virtual void transmitRoutine();		//Rtos ile isletilcek. DMA kullanilcak.
	virtual void recieveRoutine();		//Rtos ile kontrol edilcek. DMA tarafından doldurulcak.
	void errorHandle();
private:
	int8_t *_transmitBuffer;			//Dinamik
	int8_t *_recieveBuffer;			//
	uint8_t _transmitBufferLen;
	uint8_t _recieveBufferLen;
protected:
	virtual void hardwareInit();
};

#endif /* HC05_H_ */
